# Manox.Vineyards-Planter

Manox.Vineyards Planter is a small editor utility, it can populate given area with lines of plants (vine). This project doesn't contain any 3D models. All used models in video and screenshots are from fantastic package [Vine Bush Package](https://www.assetstore.unity3d.com/en/#!/content/8876)

Instalation
For demo scene, you will need [Vine Bush Package](https://www.assetstore.unity3d.com/en/#!/content/8876) and Standard Assets (Environment)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/XV4IV12Hw4g/0.jpg)](https://www.youtube.com/watch?v=XV4IV12Hw4g)

