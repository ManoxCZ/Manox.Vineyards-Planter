﻿using UnityEngine;


namespace Manox.Tools.Geometry
{
    /// <summary>
    /// Segment 3D.
    /// </summary>
    public class Segment3D
    {
        /// <summary>Start point.</summary>
        public Vector3 StartPoint;
        /// <summary>End point.</summary>
        public Vector3 EndPoint;


        /// <summary>
        /// Cosntructor.
        /// </summary>
        public Segment3D()
        {
            this.StartPoint = Vector3.zero;
            this.EndPoint = Vector3.zero;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="endPosition"></param>
        public Segment3D(Vector3 startPosition, Vector3 endPosition)
        {
            this.StartPoint = startPosition;
            this.EndPoint = endPosition;
        }
        
        /// <summary>
        /// Distance in XZ.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public float DistanceInXZ(Vector3 point)
        {
            Vector2 ab = new Vector2(this.EndPoint.x - this.StartPoint.x, this.EndPoint.z - this.StartPoint.z);

            float t = Vector2.Dot(new Vector2(point.x - this.StartPoint.x, point.z - this.StartPoint.z), ab) /
                Vector2.Dot(ab, ab);

            t = Mathf.Max(0.0f, Mathf.Min(1.0f, t));

            Vector3 temp = this.StartPoint + t * (this.EndPoint - this.StartPoint) - point;

            temp.y = 0;

            return temp.magnitude;
        }
    }
}
