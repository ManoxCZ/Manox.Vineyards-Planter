﻿using UnityEngine;
using System.Collections.Generic;
using Manox.Tools.Geometry;


namespace Manox.Tools.Vineyards
{
    /// <summary>
    /// Vineyard class.
    /// </summary>
    [System.Serializable]
    public class Vineyard : MonoBehaviour
    {
        /// <summary>Plant prototype.</summary>
        [SerializeField]
        public GameObject PlantPrototype;
        /// <summary>Border points.</summary>        
        [SerializeField]
        public List<Vector3> Borders;
        /// <summary>Lines distance.</summary>
        [SerializeField]
        public float LinesDistance;
        /// <summary>Plants distance.</summary>
        [SerializeField]
        public float PlantsDistance;
        /// <summary>Position randomization.</summary>
        [SerializeField]
        public float PositionRandomization;
        /// <summary>Min size.</summary>
        [SerializeField]
        public float MinSize;
        /// <summary>Max size.</summary>
        [SerializeField]
        public float MaxSize;
        /// <summary>Angle.</summary>
        [SerializeField]
        public float Angle;
        /// <summary>Bounding center.</summary>        
        public Vector3 BoundingCenter;
        /// <summary>Bounding radius.</summary>        
        public float BoundingRadius;
        /// <summary>Field texture.</summary>
        [SerializeField]
        public Texture2D FieldTexture;
        /// <summary>FIeld distance.</summary>
        [SerializeField]
        public float FieldDistance;
        /// <summary>Use Unity3D terrain.</summary>
        [SerializeField]
        public bool UseUnityTerrain;


        /// <summary>
        /// Constructor.
        /// </summary>
        public Vineyard()
        {
            this.BoundingCenter = Vector3.zero;
            this.BoundingRadius = 0.0f;
            this.LinesDistance = 5.0f;
            this.PlantsDistance = 2.0f;
            this.PositionRandomization = 0.2f;
            this.MinSize = 0.8f;
            this.MaxSize = 1.2f;
            this.FieldDistance = 0.5f;
            this.UseUnityTerrain = true;
        }

        /// <summary>
        /// Start.
        /// </summary>
        public void Start()
        {
            this.GetBoundingSphere();
        }

        /// <summary>
        /// On destroy.
        /// </summary>
        public void OnDestroy()
        {
            this.DeleteInstances();
        }

        /// <summary>
        /// Vertex added.
        /// </summary>        
        /// <param name="vertex"></param>
        public void VertexAdded(Vector3 vertex)
        {
            this.DeleteInstances();

            this.Borders.Add(vertex);            

            this.PlantTrees();
        }

        /// <summary>
        /// Update vertex.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="vertex"></param>
        public void UpdateVertex(int index, Vector3 vertex)
        {
            this.DeleteInstances();

            this.Borders[index] = vertex;

            this.PlantTrees();
        }

        /// <summary>
        /// Remove vertex.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="vertex"></param>
        public void RemoveVertex(int index)
        {
            this.DeleteInstances();

            this.Borders.RemoveAt(index);

            this.PlantTrees();
        }

        /// <summary>
        /// Update vertex.
        /// </summary>
        /// <param name="angle"></param>        
        public void UpdateAngle(float angle)
        {
            this.DeleteInstances();

            this.Angle = angle;

            this.PlantTrees();
        }

        /// <summary>
        /// Update lines distance.
        /// </summary>
        /// <param name="linesDistance"></param>        
        public void UpdateLinesDistance(float linesDistance)
        {
            this.DeleteInstances();

            this.LinesDistance = linesDistance;

            this.PlantTrees();
        }

        /// <summary>
        /// Update plants distance.
        /// </summary>
        /// <param name="plantsDistance"></param>        
        public void UpdatePlantsDistance(float plantsDistance)
        {
            this.DeleteInstances();

            this.PlantsDistance = plantsDistance;

            this.PlantTrees();
        }

        /// <summary>
        /// Update plant prototype.
        /// </summary>
        /// <param name="plantPrototype"></param>        
        public void UpdatePlantPrototype(GameObject plantPrototype)
        {
            this.DeleteInstances();

            this.PlantPrototype = plantPrototype;

            this.PlantTrees();
        }

        /// <summary>
        /// Update position randomization.
        /// </summary>
        /// <param name="newValue"></param>        
        public void UpdatePositionRandomization(float newValue)
        {
            this.DeleteInstances();

            this.PositionRandomization = newValue;

            this.PlantTrees();
        }

        /// <summary>
        /// Update min size.
        /// </summary>
        /// <param name="newValue"></param>        
        public void UpdateMinSize(float newValue)
        {
            this.DeleteInstances();

            this.MinSize = newValue;

            this.PlantTrees();
        }

        /// <summary>
        /// Update max size.
        /// </summary>
        /// <param name="newValue"></param>        
        public void UpdateMaxSize(float newValue)
        {
            this.DeleteInstances();

            this.MaxSize = newValue;

            this.PlantTrees();
        }

        /// <summary>
        /// Update max size.
        /// </summary>
        /// <param name="newValue"></param>        
        public void UpdateUseUnityTerrain(bool newValue)
        {
            this.DeleteInstances();

            this.UseUnityTerrain = newValue;

            this.PlantTrees();
        }

        /// <summary>
        /// Delete instance for terrain.
        /// </summary>
        public void DeleteInstances()
        {
            if (this.PlantPrototype == null)
                return;

            if (this.UseUnityTerrain)
            {
                Terrain[] terrains = FindObjectsOfType<Terrain>();

                foreach (Terrain terrain in terrains)
                    this.DeleteInstancesForTerrain(terrain);
            }
            else
            {
                for (int i = this.transform.childCount - 1; i >= 0; i--)
                    DestroyImmediate(this.transform.GetChild(i).gameObject);
            }
        }

        /// <summary>
        /// Delete instances for terrain.
        /// </summary>
        /// <param name="terrain"></param>
        private void DeleteInstancesForTerrain(Terrain terrain)
        {            
            int plantInstanceId = -1;

            for (int i = 0; i < terrain.terrainData.treePrototypes.Length; i++)
                if (terrain.terrainData.treePrototypes[i].prefab == this.PlantPrototype)
                    plantInstanceId = i;

            if (plantInstanceId == -1)
                return;


            List<TreeInstance> treeInstances = new List<TreeInstance>(terrain.terrainData.treeInstances);

            int index = 0;
            
            while (index < treeInstances.Count)
            {
                if (treeInstances[index].prototypeIndex == plantInstanceId &&
                    this.PointInPolygon(new Vector3(
                        treeInstances[index].position.x * terrain.terrainData.size.x,
                        0,
                        treeInstances[index].position.z * terrain.terrainData.size.z)))
                    treeInstances.RemoveAt(index);
                else
                    index++;
            }

            terrain.terrainData.treeInstances = treeInstances.ToArray();

            terrain.Flush();
        }

        /// <summary>
        /// Plant trees.
        /// </summary>
        public void PlantTrees()
        {
            this.GetBoundingSphere();

            if (this.PlantPrototype == null)
                return;

            if (this.UseUnityTerrain)
            {
                Terrain[] terrains = FindObjectsOfType<Terrain>();

                foreach (Terrain terrain in terrains)
                    this.PlantTrees(terrain);
            }
            else
            {
                this.PlantTreesAsChildren();
            }
        }

        /// <summary>
        /// Plant trees.
        /// </summary>
        private void PlantTrees(Terrain terrain)
        {           
            int plantInstanceId = -1;

            for (int i = 0; i < terrain.terrainData.treePrototypes.Length; i++)
                if (terrain.terrainData.treePrototypes[i].prefab == this.PlantPrototype)
                    plantInstanceId = i;

            if (plantInstanceId == -1)
            {
                plantInstanceId = terrain.terrainData.treePrototypes.Length;

                TreePrototype[] treePrototypes = terrain.terrainData.treePrototypes;

                System.Array.Resize<TreePrototype>(ref treePrototypes, plantInstanceId + 1);
                treePrototypes[plantInstanceId] = new TreePrototype();
                treePrototypes[plantInstanceId].prefab = this.PlantPrototype;

                terrain.terrainData.treePrototypes = treePrototypes;
            }            

            int linesCount = (int)(this.BoundingRadius / this.LinesDistance);
            int plantsLineCount = (int)(this.BoundingRadius / this.PlantsDistance);

            List<TreeInstance> treeInstances = new List<TreeInstance>(terrain.terrainData.treeInstances);

            Matrix4x4 transform = Matrix4x4.TRS(
                this.BoundingCenter,
                Quaternion.AngleAxis(this.Angle, Vector3.up),
            Vector3.one);

            for (int iLine = -linesCount; iLine <= linesCount; iLine++)
                for (int iPlant = -plantsLineCount; iPlant <= plantsLineCount; iPlant++)
                {
                    Vector3 treePosition = new Vector3(
                        (float)iPlant * this.PlantsDistance + Random.Range(- this.PositionRandomization, this.PositionRandomization),
                        0,
                        (float)iLine * this.LinesDistance + Random.Range(-this.PositionRandomization, this.PositionRandomization));

                    treePosition = transform.MultiplyPoint(treePosition);

                    if (this.PointInPolygon(treePosition))
                    {
                        TreeInstance newInstance = new TreeInstance();
                        newInstance.prototypeIndex = plantInstanceId;
                        newInstance.position = new Vector3(
                            treePosition.x / terrain.terrainData.size.x,
                            0,
                            treePosition.z / terrain.terrainData.size.z);
                        newInstance.position.y = terrain.terrainData.GetInterpolatedHeight(newInstance.position.x, newInstance.position.z) / terrain.terrainData.size.y;
                        newInstance.rotation = Random.Range(-Mathf.PI, Mathf.PI);
                        newInstance.heightScale = Random.Range(this.MinSize, this.MaxSize);
                        newInstance.widthScale = Random.Range(this.MinSize, this.MaxSize);
                        newInstance.color = Color.white;
                        newInstance.lightmapColor = Color.white;
                        
                        treeInstances.Add(newInstance);
                    }
                }

            terrain.terrainData.treeInstances = treeInstances.ToArray();

            terrain.Flush();
        }

        /// <summary>
        /// Plant trees as children.
        /// </summary>
        private void PlantTreesAsChildren()
        {
            int linesCount = (int)(this.BoundingRadius / this.LinesDistance);
            int plantsLineCount = (int)(this.BoundingRadius / this.PlantsDistance);

            Matrix4x4 transform = Matrix4x4.TRS(
                this.BoundingCenter,
                Quaternion.AngleAxis(this.Angle, Vector3.up),
            Vector3.one);

            for (int iLine = -linesCount; iLine <= linesCount; iLine++)
                for (int iPlant = -plantsLineCount; iPlant <= plantsLineCount; iPlant++)
                {
                    Vector3 treePosition = new Vector3(
                        (float)iPlant * this.PlantsDistance + Random.Range(-this.PositionRandomization, this.PositionRandomization),
                        0,
                        (float)iLine * this.LinesDistance + Random.Range(-this.PositionRandomization, this.PositionRandomization));

                    treePosition = transform.MultiplyPoint(treePosition);

                    if (this.PointInPolygon(treePosition))
                    {
                        RaycastHit hit;

                        treePosition.y = 1000;

                        Ray newRay = new Ray(treePosition, Vector3.down);

                        if (Physics.Raycast(newRay, out hit))
                            treePosition.y = hit.point.y;

                        GameObject treeInstance = GameObject.Instantiate(this.PlantPrototype,
                            treePosition, Quaternion.AngleAxis(Random.Range(0.0f, 360.0f), Vector3.up)) as GameObject;

                        treeInstance.transform.parent = this.transform;

                        treeInstance.name = this.PlantPrototype.name;
                    }
                }
        }

        /// <summary>
        /// Point in polygon.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private bool PointInPolygon(Vector3 point)
        {
            bool c = false;

            for (int i = 0, j = this.Borders.Count - 1; i < this.Borders.Count; j = i++)
            {
                if (((this.Borders[i].z >= point.z) != (this.Borders[j].z >= point.z)) &&
                    (point.x <= (this.Borders[j].x - this.Borders[i].x) * (point.z - this.Borders[i].z) / (this.Borders[j].z - this.Borders[i].z) + this.Borders[i].x))
                    c = !c;
            }

            return c;
        }

        /// <summary>
        /// Bounding sphere.
        /// </summary>
        /// <returns></returns>
        private void GetBoundingSphere()
        {
            if (this.Borders.Count == 0)
                return;


            Vector3 center = Vector3.zero;

            for (int i = 0; i < this.Borders.Count; i++)
                center += this.Borders[i];

            center /= (float)this.Borders.Count;

            float radius = 0;

            foreach (Vector3 border in this.Borders)
                radius = Mathf.Max(radius, Mathf.Max(Mathf.Abs(border.x - center.x), Mathf.Abs(border.z - center.z)));            

            this.BoundingCenter = center;
            this.BoundingRadius = radius;            
        }

        /// <summary>
        /// Bake splat maps.
        /// </summary>        
        public void BakeSplatMaps()
        {
            Terrain[] terrains = FindObjectsOfType<Terrain>();

            foreach (Terrain terrain in terrains)
                this.BakeSplatMap(terrain);
        }

        /// <summary>
        /// Bake splat map.
        /// </summary>
        /// <param name="terrain"></param>
        public void BakeSplatMap(Terrain terrain)
        {
            this.GetBoundingSphere();

            int splatPrototype = -1;

            for (int i = 0; i < terrain.terrainData.splatPrototypes.Length; i++)
                if (terrain.terrainData.splatPrototypes[i].texture == this.FieldTexture)
                    splatPrototype = i;
            
            if (splatPrototype == -1)
            {
                List<SplatPrototype> splatPrototypes = new List<SplatPrototype>(terrain.terrainData.splatPrototypes);

                SplatPrototype newPrototype = new SplatPrototype();
                newPrototype.texture = this.FieldTexture;
                newPrototype.tileSize = new Vector2(5, 5);

                splatPrototypes.Add(newPrototype);
                
                terrain.terrainData.splatPrototypes = splatPrototypes.ToArray();
            }


            float tempX = terrain.terrainData.size.x / terrain.terrainData.alphamapWidth;
            float tempY = terrain.terrainData.size.z / terrain.terrainData.alphamapHeight;

            int startX = Mathf.Max(0, (int)((this.BoundingCenter.x - this.BoundingRadius) / tempX) - 1);
            int endX = Mathf.Min(terrain.terrainData.alphamapWidth - 1, (int)((this.BoundingCenter.x + this.BoundingRadius) / tempX) + 1);

            int startY = Mathf.Max(0, (int)((this.BoundingCenter.z - this.BoundingRadius) / tempY) - 1);
            int endY = Mathf.Min(terrain.terrainData.alphamapHeight - 1, (int)((this.BoundingCenter.z + this.BoundingRadius) / tempY) + 1);
            

            float[,,] alphamaps = terrain.terrainData.GetAlphamaps(startX, startY, endX - startX, endY - startY);

            List<Segment3D> segments = this.GetVineyardSegments(terrain);

            for (int iRow = 0; iRow < alphamaps.GetLength(0); iRow++)
                for (int iCol = 0; iCol < alphamaps.GetLength(1); iCol++)
                    if (this.GetClosestDistanceToVineyardSegment(segments,
                        new Vector3((startX + iCol) * tempX, 0, (startY + iRow) * tempY)) < this.FieldDistance)
                        for (int iSplat = 0; iSplat < alphamaps.GetLength(2); iSplat++)
                        {
                            if (iSplat == splatPrototype)
                                alphamaps[iRow, iCol, iSplat] = 1.0f;
                            else
                                alphamaps[iRow, iCol, iSplat] = 0.0f;
                        }

            terrain.terrainData.SetAlphamaps(startX, startY, alphamaps);

            terrain.Flush();
        }

        /// <summary>
        /// Remove detail objects in lines.
        /// </summary>
        public void RemoveDetailObjectsInLines()
        {
            this.GetBoundingSphere();

            Terrain[] terrains = FindObjectsOfType<Terrain>();

            foreach (Terrain terrain in terrains)
                this.RemoveDetailObjectsInLines(terrain);
        }

        /// <summary>
        /// Remove detail objects in lines.
        /// </summary>
        /// <param name="terrain"></param>
        public void RemoveDetailObjectsInLines(Terrain terrain)
        {
            DetailPrototype[] dPrototypes = terrain.terrainData.detailPrototypes;

            float tempX = terrain.terrainData.size.x / terrain.terrainData.detailWidth;
            float tempY = terrain.terrainData.size.z / terrain.terrainData.detailHeight;

            int startX = Mathf.Max(0, (int)((this.BoundingCenter.x - this.BoundingRadius) / tempX) - 1);
            int endX = Mathf.Min(terrain.terrainData.detailWidth - 1, (int)((this.BoundingCenter.x + this.BoundingRadius) / tempX) + 1);

            int startY = Mathf.Max(0, (int)((this.BoundingCenter.z - this.BoundingRadius) / tempY) - 1);
            int endY = Mathf.Min(terrain.terrainData.detailHeight - 1, (int)((this.BoundingCenter.z + this.BoundingRadius) / tempY) + 1);
            
            List<Segment3D> segments = this.GetVineyardSegments(terrain);

            for (int iProt = 0; iProt < dPrototypes.Length; iProt++)
            {
                int[,] details = terrain.terrainData.GetDetailLayer(startX, startY, endX - startX, endY - startY, iProt);

                for (int iRow = 0; iRow < details.GetLength(0); iRow++)
                    for (int iCol = 0; iCol < details.GetLength(1); iCol++)
                        if (this.GetClosestDistanceToVineyardSegment(segments,
                            new Vector3((startX + iCol) * tempX, 0, (startY + iRow) * tempY)) < this.FieldDistance)
                            details[iRow, iCol] = 0;

                terrain.terrainData.SetDetailLayer(startX, startY, iProt, details);
            }

            terrain.Flush();
        }

        /// <summary>
        /// Get vineyard segments.
        /// </summary>
        /// <param name="terrain"></param>
        /// <returns></returns>
        private List<Segment3D> GetVineyardSegments(Terrain terrain)
        {
            List<Segment3D> output = new List<Segment3D>();

            int linesCount = (int)(this.BoundingRadius / this.LinesDistance);
            int plantsLineCount = (int)(this.BoundingRadius / this.PlantsDistance);

            Matrix4x4 transform = Matrix4x4.TRS(
                this.BoundingCenter,
                Quaternion.AngleAxis(this.Angle, Vector3.up),
            Vector3.one);
            
            for (int iLine = -linesCount; iLine <= linesCount; iLine++)
            {
                bool lineStarted = false;
                Vector3 startPoint = Vector3.zero;
                Vector3 endPoint = Vector3.zero;

                for (int iPlant = -plantsLineCount; iPlant <= plantsLineCount; iPlant++)
                {
                    endPoint = new Vector3(
                        (float)iPlant * this.PlantsDistance,
                        0,
                        (float)iLine * this.LinesDistance);

                    endPoint = transform.MultiplyPoint(endPoint);

                    bool isInPolygon = this.PointInPolygon(endPoint);
                    
                    if (!lineStarted && isInPolygon)
                    {
                        lineStarted = true;

                        startPoint = endPoint;
                    }

                    if (lineStarted && !isInPolygon)
                    {
                        lineStarted = false;

                        output.Add(new Segment3D(startPoint, endPoint));
                    }
                }

                if (lineStarted)
                    output.Add(new Segment3D(startPoint, endPoint));                
            }

            return output;
        }

        /// <summary>
        /// Get closest distance to vineyard segment.
        /// </summary>
        /// <param name="segments"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private float GetClosestDistanceToVineyardSegment(List<Segment3D> segments, Vector3 point)
        {
            float closestDist = float.MaxValue;

            foreach (Segment3D segment in segments)
            {
                float dist = segment.DistanceInXZ(point);

                if (dist < closestDist)
                    closestDist = dist;                
            }

            return closestDist;
        }
    }
}