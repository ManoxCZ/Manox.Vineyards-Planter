﻿using UnityEditor;
using UnityEngine;


namespace Manox.Tools.Vineyards
{
    /// <summary>
    /// Powerline editor.
    /// </summary>
    [CustomEditor(typeof(Vineyard))]
    public class VineyardEditor : Editor
    {
        /// <summary>Vineyard.</summary>
        public Vineyard Vineyard { get { return this.target as Vineyard; } }
        
        /// <summary>
        /// Create powerline.
        /// </summary>
        [MenuItem("GameObject/Manox Tools/Vineyards/Vineyard", false, 25)]
        public static void CreateVineayrd()
        {
            GameObject newVineayrd = GameObject.Instantiate(
               AssetDatabase.LoadAssetAtPath(@"Assets/Manox.Tools/Manox.Vineyards/Prefabs/Vineyard.prefab", typeof(GameObject))) as GameObject;            

            //GameObject newVineayrd = GameObject.Instantiate(Resources.Load("Manox.Vineyard"),
            //    //AssetDatabase.LoadAssetAtPath(@"Assets/Manox.Tools/Manox.Vineyards/Prefabs/Vineyard.prefab", typeof(GameObject)),
            //    Vector3.zero, Quaternion.identity) as GameObject;

            newVineayrd.name = "Vineyard";
            newVineayrd.transform.position = Vector3.zero;
            newVineayrd.transform.rotation = Quaternion.identity;

            Selection.activeGameObject = newVineayrd as GameObject;
        }

        /// <summary>
        /// On inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            //bool useUnityTerrainTemp = EditorGUILayout.Toggle("Use Unity3D terrain", this.Vineyard.UseUnityTerrain);

            //if (useUnityTerrainTemp != this.Vineyard.UseUnityTerrain)
            //    this.Vineyard.UpdateUseUnityTerrain(useUnityTerrainTemp);

            EditorGUILayout.Separator();

            GameObject protTemp = 
                EditorGUILayout.ObjectField("Plant prototype", this.Vineyard.PlantPrototype, typeof(GameObject), true) as GameObject;

            if (protTemp != this.Vineyard.PlantPrototype)
                this.Vineyard.UpdatePlantPrototype(protTemp);

            float linesDistTemp = EditorGUILayout.FloatField("Lines distance", this.Vineyard.LinesDistance);

            if (linesDistTemp != this.Vineyard.LinesDistance)
                this.Vineyard.UpdateLinesDistance(linesDistTemp);

            float plantsDistTemp = EditorGUILayout.FloatField("Plants distance", this.Vineyard.PlantsDistance);

            if (plantsDistTemp != this.Vineyard.PlantsDistance)
                this.Vineyard.UpdatePlantsDistance(plantsDistTemp);

            float randomPositionTemp = EditorGUILayout.FloatField("Random position offset", this.Vineyard.PositionRandomization);

            if (randomPositionTemp != this.Vineyard.PositionRandomization)
                this.Vineyard.UpdatePositionRandomization(randomPositionTemp);

            float minSizeTemp = EditorGUILayout.FloatField("Min size factor", this.Vineyard.MinSize);

            if (minSizeTemp != this.Vineyard.MinSize)
                this.Vineyard.UpdateMinSize(minSizeTemp);

            float maxSizeTemp = EditorGUILayout.FloatField("Max size factor", this.Vineyard.MaxSize);

            if (maxSizeTemp != this.Vineyard.MaxSize)
                this.Vineyard.UpdateMaxSize(maxSizeTemp);

            if (this.Vineyard.UseUnityTerrain)
            {
                EditorGUILayout.Separator();

                GUILayout.Label("Splat maps generator");

                this.Vineyard.FieldTexture = EditorGUILayout.ObjectField("Field texture", this.Vineyard.FieldTexture, typeof(Texture2D), true) as Texture2D;
                this.Vineyard.FieldDistance = EditorGUILayout.FloatField("Field distance", this.Vineyard.FieldDistance);

                if (GUILayout.Button("Bake splat maps"))
                {
                    Terrain[] terrains = FindObjectsOfType<Terrain>();

                    Undo.RecordObjects(terrains, "Bake splat maps");

                    this.Vineyard.BakeSplatMaps();
                }

                EditorGUILayout.Separator();

                GUILayout.Label("Remove detail objects");

                if (GUILayout.Button("Remove detail objects"))
                {
                    Terrain[] terrains = FindObjectsOfType<Terrain>();

                    Undo.RecordObjects(terrains, "Remove detail objects");

                    this.Vineyard.RemoveDetailObjectsInLines();
                }
            }
        }

        /// <summary>
        /// On Scene GUI.
        /// </summary>
        public void OnSceneGUI()
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            if (Event.current.control &&
                Event.current.type == EventType.MouseDown &&
                Event.current.button == 0)
            {
                Ray worldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

                RaycastHit hitInfo;

                if (Physics.Raycast(worldRay, out hitInfo))
                    this.Vineyard.VertexAdded(hitInfo.point);
            }
            
            Vector3 temp = Vector3.zero;

            /// Draw.
            for (int i = 0; i < this.Vineyard.Borders.Count; i++)
            {
                GUI.SetNextControlName(i.ToString());

                temp = Handles.FreeMoveHandle(this.Vineyard.Borders[i], Quaternion.identity, 1.0f, new Vector3(0f, 0f, 0f), Handles.SphereCap);

                if (temp != this.Vineyard.Borders[i])
                {
                    RaycastHit hit;

                    temp.y = 1000.0f;

                    Ray newRay = new Ray(temp, Vector3.down);

                    if (Physics.Raycast(newRay, out hit))
                        temp.y = hit.point.y;

                    this.Vineyard.UpdateVertex(i, temp);
                }
            }

            if (Event.current != null &&
                Event.current.isKey &&
                Event.current.keyCode == KeyCode.Delete &&
                !string.IsNullOrEmpty(GUI.GetNameOfFocusedControl()))
            {
                int index = int.Parse(GUI.GetNameOfFocusedControl());

                this.Vineyard.RemoveVertex(index);
            }


            // Angle.
            Handles.color = Color.cyan;            
            
            Quaternion originalRot = Quaternion.AngleAxis(this.Vineyard.Angle, Vector3.up);

            Quaternion rot = Handles.Disc(originalRot, 
                this.Vineyard.BoundingCenter, new Vector3(0, 1, 0), 5, false, 0);

            if (rot != originalRot)
                this.Vineyard.UpdateAngle(rot.eulerAngles.y);
            
            // Draw borders.
            Handles.color = Color.cyan;

            if (this.Vineyard.Borders.Count > 1)
                for (int i = 0; i < this.Vineyard.Borders.Count; i++)
                    Handles.DrawLine(
                        this.Vineyard.Borders[i],
                        this.Vineyard.Borders[(i + 1) % this.Vineyard.Borders.Count]);
        }
    }
}